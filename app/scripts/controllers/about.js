'use strict';

/**
 * @ngdoc function
 * @name azureDemoTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the azureDemoTestApp
 */
angular.module('azureDemoTestApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
