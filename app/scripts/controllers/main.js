'use strict';

/**
 * @ngdoc function
 * @name azureDemoTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the azureDemoTestApp
 */
angular.module('azureDemoTestApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
